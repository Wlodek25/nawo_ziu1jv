package com.example.controllers;

import com.example.entities.Rider;
import com.example.entities.Simple;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * Created by pawl on 24/09/16.
 */
@Controller
class SimpleController {

	@RequestMapping("/simple/{name}")
	public String hello(@PathVariable String name, Map<String, Object> model) {
		Simple simple = new Simple("John", "\"Dynamically displayed last name taken from URL\" " + name );

		model.put("simple", simple);

		return "simpledetailpage";
	}
}