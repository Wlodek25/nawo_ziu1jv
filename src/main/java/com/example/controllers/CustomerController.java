package com.example.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.entities.Customer;

@RestController
public class CustomerController {

    private static final String template = "Hello, %s!";

    @RequestMapping("/customers")
    public Customer greeting(@RequestParam(value="firstName", defaultValue="Guest") String firstName,
                             @RequestParam(value="lastName", defaultValue="") String lastName) {
        return new Customer(firstName, lastName);
    }
}
