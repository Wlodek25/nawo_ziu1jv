package com.example.controllers;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.entities.Rider;

/**
 * Created by pawl on 24/09/16.
 */
@Controller
class GreetingController {

	@RequestMapping("/hello/{name}")
	public String hello(@PathVariable String name, Map<String, Object> model) {
		Rider rider = new Rider("John", name,
				LocalDate.parse("1990-01-01", DateTimeFormat.forPattern("yyyy-MM-dd")));

		model.put("rider", rider);

		return "redirect:/views/riderdetailpage";
	}
}