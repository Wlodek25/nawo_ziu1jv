package com.example.entities;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Simple {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    public String getFullName() {
        return this.firstName + this.lastName + "(value returned by getter getFisrtName() which is automagically called by JSP/JSTL views";
    }

    public String getFirstName() {
        return this.firstName;
    }
    public String getLastName() {
        return this.lastName;
    }

    private String firstName;
    private String lastName;

    protected Simple() {}

    public Simple(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;

    }
}