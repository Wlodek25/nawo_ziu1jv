<%@tag description="Main Page template with concrete header and footer" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="mpt" tagdir="/WEB-INF/tags/simplepage" %>

<t:genericpage>
 <jsp:attribute name="header">
    <mpt:simplepageheader/>
 </jsp:attribute>
 <jsp:attribute name="footer">
      <mpt:simplepagefooter/>
   </jsp:attribute>
 <jsp:body>
    <p>Static content from simplepage.tag file</p>
    <p>
        <mpt:simpledetail simple="${simple}"/>
    </p>
 </jsp:body>
 </t:genericpage>
