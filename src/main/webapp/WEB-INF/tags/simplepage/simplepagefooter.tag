<%@tag description="Simple page specific footer" pageEncoding="UTF-8"%>

<div class="row">
   <div class="col-lg-12">
       <p>Site Specific Footer &copy; Your Website 2016</p>
   </div>
</div>
