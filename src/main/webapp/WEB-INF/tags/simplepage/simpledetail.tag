<%@tag description="Rider Detail template" pageEncoding="UTF-8"%>
<%@tag import="com.example.entities.Simple" %>
<%@attribute name="simple" required="true" type="com.example.entities.Simple"%>

First Name: ${simple.firstName} <br/>
Last Name: ${simple.lastName} <br/>
Full Name (combined first and last name in model getter): ${simple.fullName} <br/>