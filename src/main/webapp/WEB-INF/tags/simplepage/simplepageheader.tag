<%@tag description="Reusable header for main page" pageEncoding="UTF-8"%>


<!-- Full Width Image Header -->
<header class="header-image">
   <div class="headline">
       <div class="container">
           <h1>#SuperEnduro</h1>
           <h2>READY TO RACE.</h2>
       </div>
   </div>
</header>
