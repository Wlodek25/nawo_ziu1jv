<%@tag description="Rider Detail template" pageEncoding="UTF-8"%>
<%@tag import="com.example.Rider" %>
<%@attribute name="rider" required="true" type="com.example.Rider"%>

First Name: ${rider.firstName} <br/>
Last Name: ${rider.lastName} <br/>
Age: ${rider.age}<br/>
Date of Birth: ${rider.dob}<br/>