<%@tag description="Generic Page Template" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>

<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">

      <title>#READY TO RACE</title>
      <!-- Bootstrap Core CSS -->
      <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">

      <!-- Custom CSS -->
      <link href="<c:url value="/resources/css/one-page-wonder.css" />" rel="stylesheet">

      <script src="<c:url value="/resources/js/jquery.js" />"></script>
      <script src="<c:url value="/resources/js/bootstrap.js" />"></script>
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>

  <body>




    <div id="pageheader">
      <jsp:invoke fragment="header"/>
    </div>

    <t:navbar/>

    <div id="content">
      <jsp:doBody/>
    </div>
    <div id="pagefooter">
      <jsp:invoke fragment="footer"/>
    </div>
    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; Your Website 2016</p>
            </div>
        </div>
    </footer>
  </body>
</html>