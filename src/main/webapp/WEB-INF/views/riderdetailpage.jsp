<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:riderpage riderName="${rider.fullName}">
  <p>
    <t:riderdetail rider="${rider}"/>
  </p>
</t:riderpage>