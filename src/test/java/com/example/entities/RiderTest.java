package com.example.entities;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import org.joda.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.example.entities.Rider;


/**
 * Created by pawl on 28/09/16.
 */


@RunWith(PowerMockRunner.class)
@PrepareForTest(LocalDate.class)
public class RiderTest {

    @Test
    public void riderAgeEvalution() {
        int day = 1, month = 1, yearNow = 2000, yearRiderDateOfBirth = 1990,
            expectedRiderAge;

        LocalDate riderDateOfBirth = new LocalDate(yearRiderDateOfBirth, month, day);
        LocalDate now = new LocalDate(yearNow, month, day);
        expectedRiderAge = yearNow - yearRiderDateOfBirth;
        // Mock LocalDate.now() to return constant value
        PowerMockito.mockStatic(LocalDate.class);
        when(LocalDate.now()).thenReturn(now);

        Rider rider = new Rider("Test", "Rider", riderDateOfBirth);


        assertEquals(expectedRiderAge, rider.getAge());
    }

}
