Java EE project - README

###References
See REFERENCES.MD file to get most important 
information about Java EE, JDBC, JPA, Spring.

###Environment setup
####Requirements
* JDK 1.8
* Gradle 3.0
* PostgreSQL

####Recommended tools
* Homebrew (on Mac OS X) 

Go to cloned repository and type into terminal:  
```./gradlew idea```

Then open project in Intellij Idea CE.



###Deployment
####PostgreSQL database configuration on Mac OS X
#####Mac OS X

```
initdb /usr/local/var/postgres -E utf8

pg_ctl -D /usr/local/var/postgres -l /usr/local/var/postgres/server.log start   
```

####Create database in PostgreSQL
```
createuser <database_username>
createdb -h localhost -p 5432 -U <unix_user> -O <database_username> <database_name>
psql -U <database_usernam> <database_name>
ALTER USER <database_userame> WITH PASSWORD '<database_password>';
```

###End-user tests

###Testing


###Usage
